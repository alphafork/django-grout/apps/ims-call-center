import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from ims_base.models import AbstractBaseDetail, AbstractLog
from reusable_models import get_model_from_string

Staff = get_model_from_string("STAFF")
Designation = get_model_from_string("DESIGNATION")


class CallSheetCategory(AbstractBaseDetail):
    pass


class CallSheet(AbstractLog):
    CALLSHEET_STATUS = [
        ("li", "listed"),
        ("qu", "queued"),
        ("pe", "pending"),
        ("cl", "closed"),
        ("es", "escalated"),
    ]

    category = models.ForeignKey(CallSheetCategory, on_delete=models.CASCADE)
    details = models.TextField(blank=True)
    list_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=CALLSHEET_STATUS)
    close_date = models.DateTimeField(null=True, blank=True)
    priority = models.PositiveSmallIntegerField(null=True, blank=True)
    pool = models.ForeignKey(Designation, on_delete=models.CASCADE)
    primary_phone_no = models.CharField(max_length=15)
    secondary_phone_no = models.CharField(max_length=15, null=True, blank=True)
    is_assigned = models.BooleanField(default=False)


class CallStatus(AbstractBaseDetail):
    pass


class Call(AbstractLog):
    callsheet = models.ForeignKey(CallSheet, on_delete=models.CASCADE)
    staff = models.ForeignKey(Staff, on_delete=models.CASCADE)
    assigned_time = models.DateTimeField(auto_now_add=True)
    call_time = models.DateTimeField(null=True, blank=True)
    status = models.ForeignKey(
        CallStatus, on_delete=models.CASCADE, null=True, blank=True
    )
    feedback = models.TextField(blank=True)
    remarks = models.TextField(blank=True)
    is_active = models.BooleanField(default=False)


@receiver(post_save, sender=CallSheet)
def update_on_close_status(instance, **kwargs):
    if instance.status == "cl":
        instance.call_set.filter(is_active=True).update(is_active=False)
        if instance.is_assigned:
            instance.is_assigned = False
            instance.save()
        if not instance.close_date:
            instance.close_date = datetime.datetime.now()
            instance.save()
