from django.apps import AppConfig


class IMSCallCenterConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_call_center"
