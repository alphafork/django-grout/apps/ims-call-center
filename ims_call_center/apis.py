import datetime

from ims_base.apis import BaseAPIViewSet
from rest_framework.filters import BaseFilterBackend
from reusable_models import get_model_from_string

from ims_call_center.models import Call
from ims_call_center.serializers import CallSerializer

StaffDesignation = get_model_from_string("STAFF_DESIGNATION")


class CallFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}

        for filter_str in ["callsheet", "staff", "status", "is_active"]:
            if filter_str in request.query_params:
                filter_map.update(
                    {
                        filter_str: request.query_params[filter_str],
                    }
                )

        return queryset.filter(**filter_map)


class CallAPI(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [CallFilter]
    serializer_class = CallSerializer
    queryset = Call.objects.order_by("-call_time")


class CallSheetAPI(BaseAPIViewSet):
    def get_queryset(self):
        staff_designation = StaffDesignation.objects.filter(
            staff__user=self.request.user,
            is_default=True,
            is_active=True,
        )
        if staff_designation.exists() and staff_designation.count() == 1:
            designation = staff_designation.get().designation
            if designation.name == "Admin":
                return super().get_queryset()
            if designation.name == "Call Center Agent":
                assigned = Call.objects.filter(
                    staff__user=self.request.user,
                    is_active=True,
                )
                new_callsheets = self.model_class.objects.filter(
                    is_assigned=False,
                    pool=designation,
                    list_date__lte=datetime.date.today(),
                    status__in=["li", "qu"],
                )
                fetch_count = assigned.count()

                callsheet_ids = list(
                    new_callsheets.values_list("id", flat=True)[: (10 - fetch_count)]
                ) + list(assigned.values_list("callsheet", flat=True))

                callsheets = self.model_class.objects.filter(id__in=callsheet_ids)
                for callsheet in new_callsheets:
                    Call.objects.create(
                        callsheet=callsheet,
                        staff=self.request.user.staff,
                        is_active=True,
                    )
                    callsheet.is_assigned = True
                    callsheet.list_date = datetime.date.today()
                    callsheet.save()
                return callsheets
            else:
                return super().get_queryset().filter(pool=designation)
        else:
            return self.model_class.objects.none()
