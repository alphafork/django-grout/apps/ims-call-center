import datetime

from ims_base.serializers import BaseModelSerializer
from ims_workday.services import get_adjacent_workday
from rest_framework import serializers
from reusable_models import get_model_from_string

Designation = get_model_from_string("DESIGNATION")


class CallSerializer(BaseModelSerializer):
    CALLSHEET_STATUS = [
        ("ca", "call later"),
        ("re", "relist"),
        ("cl", "closed"),
        ("es", "escalated"),
    ]
    callsheet_status = serializers.ChoiceField(
        required=False,
        write_only=True,
        choices=CALLSHEET_STATUS,
    )

    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["is_active"]

    def create(self, validated_data):
        validated_data.pop("callsheet_status", None)
        return super().create(validated_data)

    def update(self, instance, validated_data):
        callsheet_status = validated_data.pop("callsheet_status", None)
        callsheet = validated_data.get("callsheet")
        if callsheet_status:
            if callsheet_status != "ca":
                callsheet.is_assigned = False
                validated_data["is_active"] = False
                if callsheet_status == "re":
                    callsheet.status = "qu"
                    next_work_day = get_adjacent_workday(
                        "succeeding",
                        (datetime.date.today() + datetime.timedelta(1)),
                    )
                    callsheet.list_date = next_work_day
                if callsheet_status == "cl":
                    callsheet.close_date = datetime.date.today()
                    callsheet.status = callsheet_status
                if callsheet_status == "es":
                    if callsheet.category.name == "Overdue":
                        callsheet.pool = Designation.objects.get(name="Accountant")
                    if callsheet.category.name == "Absentee":
                        callsheet.pool = Designation.objects.get(name="Manager")
            callsheet.save()
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["is_active"] = instance.is_active
        return data


class CallSheetSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["is_assigned"]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["is_assigned"] = instance.is_assigned
        return data
